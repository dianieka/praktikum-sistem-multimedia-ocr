# Produk Berbasis Video

# No 1

### Aplikasi Hand Gesture Recognition

Deskripsi: 

Hand gesture recognition adalah proses pengenalan dan pemahaman gerakan tangan manusia melalui penggunaan teknologi komputer. Tujuannya adalah untuk mengidentifikasi dan menginterpretasikan gerakan tangan yang dilakukan oleh seseorang, dan menerjemahkannya menjadi perintah atau tindakan yang dapat dimengerti oleh sistem komputer. Produk ini menggunakan kamera untuk mengambil gambar tangan pengguna dan kemudian melakukan pengolahan gambar serta pengenalan gestur tangan menggunakan teknologi MediaPipe dan algoritma Support Vector Machine (SVM). 

Produk ini dapat digunakan untuk mengenali dan mengklasifikasikan berbagai gestur tangan yang ditampilkan oleh pengguna yaitu angka 1 hingga 5 serta gesture "I Love You".

Pengguna dapat mengaktifkan produk ini dengan cepat dan mulai mengenali gestur tangan hanya dengan menggunakan Command Line Interface (CLI) yang disediakan. 

List Use Case:

1. Pembelajaran Bahasa Isyarat: Produk CLI Hand Gesture Recognition dapat digunakan sebagai alat pembelajaran bahasa isyarat. Pengguna dapat menggunakan gestur tangan untuk menunjukkan angka 1 hingga 5 serta gesture "I Love You" dalam bahasa isyarat. Ini membantu pengguna untuk mempelajari dan berlatih mengenali dan menghasilkan gestur yang tepat.

2. Komunikasi Tidak Lisan: Dalam situasi di mana komunikasi lisan tidak memungkinkan, seperti dalam lingkungan bising atau saat berkomunikasi dengan orang dengan gangguan pendengaran, pengguna dapat menggunakan gestur angka 1 hingga 5 dan gesture "I Love You" melalui CLI Hand Gesture Recognition untuk berkomunikasi secara tidak lisan. Ini memungkinkan pengguna untuk menyampaikan pesan dengan mudah dan efektif.

3. Pengenalan Angka: CLI Hand Gesture Recognition dapat digunakan sebagai alat untuk mengenali dan mencatat gestur angka 1 hingga 5. Ini dapat digunakan dalam pembelajaran mengenal angka.

4. Pemantauan Kesehatan Mental: Dalam terapi atau pemantauan kesehatan mental, pengguna dapat menggunakan gesture "I Love You" melalui CLI Hand Gesture Recognition sebagai tanda afirmasi atau sebagai pengingat untuk praktik kesadaran diri. Ini dapat membantu meningkatkan suasana hati atau memberikan dukungan mental dalam situasi yang membutuhkan.

Dalam semua use case ini, penggunaan CLI Hand Gesture Recognition dalam pengenalan angka 1 hingga 5 dan gesture "I Love You" memungkinkan pengguna untuk berinteraksi dengan perangkat atau lingkungan dengan cara yang lebih intuitif, mudah, dan khusus. Produk ini membuka peluang untuk komunikasi alternatif, pembelajaran, serta meningkatkan pengalaman pengguna dalam berbagai konteks.

# No 2

Proses dan algoritma yang digunakan dalam produk aplikasi Hand Gesture Recognition dapat melibatkan beberapa tahapan yang mencakup pengenalan gestur tangan. Berikut ini adalah penjelasan umum tentang proses dan algoritma yang mungkin digunakan dalam aplikasi tersebut:

1. Pengambilan Gambar: Pada tahap ini, aplikasi Hand Gesture Recognition dimulai, dan proses pengambilan gambar tangan pengguna dimulai. Aplikasi mengakses kamera yang terhubung ke perangkat, dan setiap frame gambar yang diterima dari kamera diambil dan disimpan. Gambar-gambar ini akan digunakan sebagai data untuk proses selanjutnya.

2. Pembuatan Dataset: Setelah proses pengambilan gambar selesai, gambar-gambar yang telah diambil digunakan untuk membuat dataset. Dataset ini akan berisi gambar-gambar yang mewakili berbagai gerakan tangan yang diinginkan, seperti gestur tangan angka 1 sampai 5 dan "I Love You". Dataset ini akan disimpan dalam format yang sesuai untuk proses selanjutnya, seperti dalam bentuk file gambar atau file CSV.

3. Pelatihan Model: Dataset yang telah dibuat digunakan sebagai data latih untuk melatih model pengenalan gestur tangan. Algoritma seperti Support Vector Machine (SVM) dapat digunakan untuk melatih model ini. Selama proses pelatihan, model akan belajar mengenali pola dan fitur yang berbeda dari setiap gerakan tangan yang ada dalam dataset. Pelatihan model ini bertujuan untuk membuat model dapat mengklasifikasikan gestur tangan dengan akurasi yang tinggi.

4. Pengolahan Gambar: Sebelum gambar dapat dianalisis lebih lanjut, setiap frame gambar yang diterima dari kamera perlu diproses terlebih dahulu. Proses pengolahan gambar melibatkan konversi gambar dari format BGR ke RGB agar sesuai dengan format yang diterima oleh algoritma pengenalan gestur tangan. Selain itu, gambar juga dapat dibalik secara horizontal untuk memastikan konsistensi orientasi gerakan tangan yang dikenali.

5. Deteksi Gerakan Tangan: Pada tahap ini, aplikasi menggunakan MediaPipe untuk mengakses solusi pengenalan gestur tangan. Model tangan diinisialisasi dan diterapkan pada setiap frame gambar yang telah diproses sebelumnya. Melalui analisis dan pengolahan gambar, hasil deteksi dan estimasi landmark tangan diperoleh. Hal ini memungkinkan aplikasi untuk mendapatkan informasi tentang posisi dan orientasi tangan yang ditampilkan dalam gambar.

6. Klasifikasi Gerakan Tangan: Data hasil deteksi landmark tangan kemudian diproses lebih lanjut untuk mendapatkan fitur yang relevan. Fitur-fitur ini menjadi input untuk model pengenalan gestur tangan yang telah dilatih sebelumnya. Model akan mengklasifikasikan gerakan tangan berdasarkan fitur-fitur tersebut. Misalnya, model dapat mengenali dan membedakan antara gerakan tangan angka 1 sampai 5 atau gerakan "I Love You".

7. Output dan Tampilan: Setelah gerakan tangan terdeteksi dan diklasifikasikan, aplikasi memberikan output yang sesuai. Pada tampilan aplikasi, angka 1 sampai 5 atau teks "I Love You" ditampilkan sesuai dengan gerakan tangan yang dikenali. Output ini memungkinkan pengguna untuk berinteraksi dengan aplikasi atau perangkat lain menggunakan gestur tangan mereka. Misalnya, pengguna dapat mengontrol perangkat atau menjalankan perintah tertentu berdasarkan gestur tangan yang dikenali.

# No 3

![Demo CLI - Hand Gesture Recognition](https://gitlab.com/dianieka/praktikum-sistem-multimedia-ocr/-/raw/main/Hand_Gesture_Recognition.gif)

Source Code : https://github.com/dianiekaputri/Hand-Gesture-Recognition-.git


# No 4

Link Youtube : https://youtu.be/QSBOCL4O3d0

# No 5

### Hand Gesture Recognition

Pada hand gesture recognition, terdapat beberapa aspek kecerdasan buatan yang terlibat. Berikut ini adalah beberapa aspek kecerdasan buatan yang relevan dalam konteks hand gesture recognition:
1. Computer Vision: Aspek ini melibatkan pemrosesan citra atau data visual yang diambil dari kamera atau sensor lainnya untuk mendapatkan informasi tentang posisi, bentuk, dan gerakan tangan. Algoritma computer vision digunakan untuk menganalisis dan mengekstrak fitur-fitur penting seperti kontur tangan, titik-titik kunci, atau representasi struktural seperti skeletal modeling.
2. Machine Learning: Pendekatan machine learning digunakan untuk melatih model yang dapat mengenali dan mengklasifikasikan gerakan tangan yang berbeda. Ini melibatkan tahap pelatihan di mana model diberikan contoh gerakan tangan yang sudah diketahui dan ditugaskan labelnya. Model ini belajar dari contoh-contoh tersebut dan kemudian dapat mengenali gerakan tangan yang belum pernah dilihat sebelumnya.
3. Deep Learning: Subbidang machine learning ini sangat relevan dalam hand gesture recognition. Deep learning dapat membantu meningkatkan akurasi dan ketahanan sistem terhadap variasi pose tangan dan kondisi pencahayaan yang berbeda.
4. Sensor Fusion: Dalam beberapa kasus, hand gesture recognition melibatkan penggunaan sensor-sensor tambahan seperti sensor inersia atau sensor presisi yang dapat mengukur gerakan tangan dengan lebih akurat. Aspek kecerdasan buatan dalam hal ini adalah menggabungkan data dari berbagai sensor untuk mendapatkan pemahaman yang lebih kaya tentang gerakan tangan dan meningkatkan akurasi pengenalan.
5. Support Vector Machines (SVM): algoritma pembelajaran mesin yang digunakan untuk masalah klasifikasi dan regresi. SVM secara umum digunakan dalam pemrosesan data yang memiliki karakteristik linier dan non-linier.

### Video Processing

Video processing adalah proses pengolahan dan analisis data video untuk memperoleh informasi yang berharga atau menghasilkan efek visual yang diinginkan. Proses ini melibatkan penggunaan teknik-teknik pengolahan citra dan pemrosesan sinyal untuk memanipulasi, mengubah, atau mengekstraksi informasi dari video.

### Video Compression

Video compression adalah proses pengurangan ukuran file video dengan tetap mempertahankan kualitas visual yang dapat diterima. Tujuan utama dari kompresi video adalah mengurangi ukuran file agar dapat dengan mudah ditransfer, disimpan, dan diputar pada perangkat dengan sumber daya terbatas.

### Alur kerja

<div class="center">

```mermaid
   flowchart TD
A[Start] --> B(Get Image)
B --> C{Image Captured?}
C --> |Yes| D[Save Image]
D --> C
C --> |No| E[Release Capture]
E --> F[Open Video Capture]
F --> G{Frame Captured?}
G --> |Yes| H[Image Processing and Classification]
H --> I[Draw Predicted Label]
I --> J[Write Frame to Output Video]
J --> G
G --> |No| K[Release Video Capture and Writer]
K --> L[End]
```
</div>


### Deskripsi Alur Kerja dalam Flowchart

Berikut adalah deskripsi alur kerja dalam flowchart di atas:

1. Langkah awal program dimulai dengan "Start".
2. Program masuk ke langkah "Pengambilan Gambar" di mana gambar tangan pengguna diambil menggunakan kamera.
3. Setelah gambar-gambar tangan berhasil diambil, program masuk ke langkah "Pembuatan Dataset" di mana gambar-gambar ini digunakan untuk membuat dataset yang mewakili berbagai gerakan tangan yang diinginkan.
4. Selanjutnya, program memasuki langkah "Pelatihan Model" di mana dataset digunakan untuk melatih model pengenalan gestur tangan, seperti SVM.
5. Setelah model dilatih, program masuk ke langkah "Pengolahan Gambar" di mana setiap frame gambar yang diterima dari kamera diproses terlebih dahulu sebelum diteruskan ke tahap selanjutnya.
6. Proses pengolahan gambar meliputi konversi gambar ke format yang sesuai, seperti BGR ke RGB, dan penerapan transformasi seperti flipping untuk memastikan konsistensi orientasi gerakan tangan.
7. Setelah gambar telah diproses, program memasuki langkah "Deteksi Gerakan Tangan" menggunakan MediaPipe untuk mendapatkan informasi tentang posisi dan orientasi tangan dalam gambar.
8. Data hasil deteksi landmark tangan kemudian diproses lebih lanjut pada langkah "Klasifikasi Gerakan Tangan" menggunakan model yang telah dilatih sebelumnya.
9. Setelah gerakan tangan diklasifikasikan, program masuk ke langkah "Output dan Tampilan" di mana hasil klasifikasi ditampilkan pada tampilan aplikasi, misalnya dengan menampilkan label prediksi pada frame.
10. Selanjutnya, program kembali ke langkah "Pengolahan Gambar" untuk memproses frame gambar berikutnya.
11. Jika tidak ada lagi frame gambar yang diterima, program mencapai "Selesai" dan kemudian "End".


# No 6

### Aplikasi Arabic Sign Language 

Deskripsi: 

Aplikasi Arabic Sign Language adalah sebuah produk yang dirancang khusus untuk membantu penyandang disabilitas tuna rungu dalam belajar huruf hijaiyah melalui Bahasa Isyarat Arab. Aplikasi ini memanfaatkan teknologi pengenalan gestur tangan dan pengolahan citra untuk menginterpretasikan Bahasa Isyarat Arab ke dalam teks Arab yang menunjukkan Bahasa Arab Standar.

Dengan menggunakan aplikasi ini, penyandang disabilitas tuna rungu dapat mempelajari huruf hijaiyah secara visual melalui representasi gestur tangan dalam Bahasa Isyarat Arab. Aplikasi ini memberikan kemampuan kepada pengguna untuk mengenali gestur tangan yang mewakili setiap huruf hijaiyah dan memahami artinya melalui teks Arab yang disajikan oleh aplikasi.

Dalam proses penggunaan aplikasi, pengguna akan melihat gestur tangan yang ditampilkan dalam bentuk video atau gambar, kemudian aplikasi akan menginterpretasikan gestur tersebut menjadi huruf hijaiyah yang sesuai dan menyajikan teks Arab. Dengan demikian, aplikasi ini memungkinkan penyandang disabilitas tuna rungu untuk belajar huruf hijaiyah dengan cara yang lebih mudah, interaktif, dan mandiri melalui Bahasa Isyarat Arab.

Aplikasi ini membantu mengatasi tantangan komunikasi dan pembelajaran yang dihadapi oleh penyandang disabilitas tuna rungu dengan memberikan mereka akses yang lebih mudah dan efektif untuk mempelajari huruf hijaiyah melalui Bahasa Isyarat Arab. Dengan adanya aplikasi ini, penyandang disabilitas tuna rungu dapat meningkatkan keterampilan mengaji mereka dan berpartisipasi dalam kegiatan belajar secara mandiri tanpa memerlukan perangkat khusus atau dukungan dari orang lain.

List Use Case:

1. Penerjemah Bahasa Isyarat: Aplikasi ini dapat digunakan sebagai alat penerjemah Bahasa Isyarat Arab ke dalam teks Arab. Ketika pengguna menunjukkan gestur tangan dalam Bahasa Isyarat Arab, aplikasi akan menerjemahkan gestur tersebut menjadi teks Arab yang dapat dipahami oleh pengguna yang tidak menguasai bahasa isyarat.

2. Pembelajaran Interaktif: Aplikasi pengenalan Bahasa Isyarat dapat digunakan sebagai alat pembelajaran huruf hijaiyah yang interaktif. Pengguna dapat melihat gestur tangan yang mewakili setiap huruf hijaiyah dan belajar mengidentifikasi dan mengingat huruf-huruf tersebut melalui Bahasa Isyarat Arab.

3. Peningkatan Keterampilan Visual: Pengenalan Bahasa Isyarat dapat membantu dalam memperkuat keterampilan visual pengguna dalam mengenali dan menginterpretasikan huruf hijaiyah. Dengan melibatkan pengguna secara visual melalui gestur tangan, aplikasi ini dapat membantu meningkatkan keterampilan visual mereka dalam mengenali bentuk dan karakteristik huruf hijaiyah.

4. Dukungan Pembelajaran Mandiri: Penggunaan pengenalan Bahasa Isyarat sebagai metode pembelajaran huruf hijaiyah memungkinkan penyandang disabilitas tuna rungu untuk belajar secara mandiri. Mereka dapat mengakses aplikasi ini kapan saja dan di mana saja, tanpa memerlukan bantuan orang lain, sehingga meningkatkan kemandirian mereka dalam proses pembelajaran.

5. Penyampaian Materi yang Lebih Menarik: Penggunaan Bahasa Isyarat dalam pengenalan huruf hijaiyah dapat membuat proses pembelajaran menjadi lebih menarik dan menyenangkan. Dalam bentuk visual dan gerakan tangan, gestur tangan yang mewakili huruf hijaiyah dapat membantu penyandang disabilitas tuna rungu untuk lebih terlibat dalam proses belajar dan meningkatkan motivasi mereka dalam mempelajari huruf hijaiyah.


# No 7

![Demo CLI - Arabic Sign Language](https://gitlab.com/dianieka/praktikum-sistem-multimedia-ocr/-/raw/main/Arabic_Sign_Language.gif)

Source Code : https://github.com/dianiekaputri/ArabicSignLanguage.git


# No 8

Link Youtube : https://youtu.be/O0zgcN6D1qU

# No 9
Proses research & development :

### Business Understanding

#### Latar belakang :

Penggunaan metode konvensional dalam pengenalan huruf hijaiyah menggunakan input berupa teks atau tulisan membutuhkan waktu yang lama, terutama ketika perintahnya kompleks. Selain itu, tidak setiap kesempatan seseorang dapat mengetik instruksi kepada komputer, misalnya ketika sedang beraktifitas atau berada dalam situasi di mana mengetik tidak memungkinkan. Hal ini menjadi tantangan dalam pembelajaran huruf hijaiyah, terutama bagi penyandang disabilitas tuna rungu yang mengandalkan bahasa isyarat sebagai metode komunikasi mereka.

#### Usulan solusi :

Untuk mengatasi masalah tersebut, salah satu solusi yang jelas adalah dengan mengembangkan model Speech to Text yang dapat mengkonversi perintah berupa suara menjadi teks. Dengan adanya model Speech to Text, pengguna dapat dengan mudah mengucapkan perintah atau huruf hijaiyah secara lisan, dan model akan mengenali dan mengubahnya menjadi teks yang dapat dipahami. Pendekatan machine learning dapat digunakan dalam pembuatan model ini, di mana model dilatih menggunakan data suara yang telah dilabeli dengan tulisan kata yang sesuai.

#### Tujuan :

Tujuan dari implementasi metode Convolutional Neural Network (CNN) dalam pengenalan bahasa isyarat sebagai metode pembelajaran huruf hijaiyah adalah untuk membangun model yang mampu mengenali dan menginterpretasikan gestur tangan dalam bahasa isyarat Arab. Model ini akan memungkinkan penyandang disabilitas tuna rungu untuk belajar huruf hijaiyah melalui bahasa isyarat dengan mudah dan interaktif. Selain itu, tujuan lainnya adalah meningkatkan kinerja dari model Speech to Text Bahasa Indonesia dalam mengenali dan mengubah perintah suara menjadi teks dengan akurasi yang tinggi. Dengan demikian, pengguna dapat lebih efektif dan efisien dalam belajar huruf hijaiyah melalui bahasa isyarat dan menggunakan perintah suara sebagai input dalam proses pembelajaran.


### Data Understanding

#### Memahami bagaimana data digunakan untuk mencapai Tujuan

Algoritma Convolutional Neural Network (CNN) digunakan untuk mengidentifikasi dan memisahkan tangan dari latar belakang dan objek lain dalam citra. CNN dilatih menggunakan dataset "The Arabic Alphabets Sign Language Dataset (ArASL)[https://data.mendeley.com/datasets/y7pckrw6z2/1]" yang berisi Dataset baru terdiri dari 54.049 gambar alfabet ArASL yang dilakukan oleh lebih dari 40 orang untuk 32 tanda dan alfabet Arab standar. Jumlah gambar per kelas berbeda-beda dari satu kelas ke kelas lainnya. Contoh gambar dari semua Tanda Bahasa Arab. Dataset ini berisi Label dari setiap Gambar Bahasa Isyarat Arab yang sesuai berdasarkan nama file gambar tersebut.

### Data Preparation

#### Melakukan berbagai prosedur penyediaan data hingga siap digunakan pada pemodelan

Proses dan algoritma yang digunakan dalam produk aplikasi Arabic Sign Language dapat melibatkan beberapa tahapan yang mencakup pengenalan gestur tangan, pemrosesan citra, dan interpretasi Bahasa Isyarat Arab. Berikut ini adalah penjelasan umum tentang proses dan algoritma yang mungkin digunakan dalam aplikasi tersebut:
1. Pengambilan Citra: Proses dimulai dengan pengambilan citra menggunakan kamera atau perangkat sensor lainnya. Citra ini akan digunakan sebagai input untuk mengenali gestur tangan dalam Bahasa Isyarat Arab.
2. Preprocessing Citra: Sebelum citra dapat digunakan untuk pengenalan gestur tangan, biasanya dilakukan tahap preprocessing. Ini melibatkan langkah-langkah seperti pengubahan ukuran citra, normalisasi intensitas warna, atau penghilangan noise untuk memastikan kualitas citra yang baik sebelum diproses lebih lanjut.
3. Deteksi Tangan: Algoritma Convolutional Neural Network (CNN) digunakan untuk mengidentifikasi dan memisahkan tangan dari latar belakang dan objek lain dalam citra.
4. Segmentasi Tangan: Setelah deteksi tangan dilakukan, tahap selanjutnya adalah segmentasi tangan dari citra.
5. Ekstraksi Fitur: Setelah segmentasi, fitur-fitur yang relevan dari gestur tangan perlu diekstraksi. Algoritma ekstraksi fitur dapat digunakan untuk mengidentifikasi pola-pola penting dalam gestur tangan, seperti posisi dan orientasi jari-jari, bentuk tangan, atau gerakan tangan. Fitur-fitur ini akan digunakan sebagai representasi numerik yang dapat diterima oleh algoritma pengenalan gestur tangan.
6. Klasifikasi Gestur: Dalam tahap akhir, algoritma klasifikasi seperti Convolutional Neural Network (CNN) akan digunakan untuk mengenali dan mengklasifikasikan gestur tangan ke dalam kategori Bahasa Isyarat Arab yang sesuai. Model CNN yang telah dilatih sebelumnya dengan dataset Bahasa Isyarat Arab akan digunakan untuk pengenalan dan klasifikasi gestur tangan.

#### Menggunakan berbagai tools dan library untuk pemrosesan data seperti:

Python :

- cmake==3.26.4
- arabic-reshaper==3.0.0
- dlib==19.24.2
- Keras==2.12.0
- numpy==1.23.5
- opencv-python==4.7.0.72
- Pillow==9.5.0
- python-bidi==0.4.2
- scipy==1.10.1
- tensorflow==2.12.0


### Modeling

1. Preprocessing Model

Tujuan utama dari pra-pemrosesan adalah untuk meningkatkan data gambar dengan mengurangi penyimpangan yang tidak diinginkan atau meningkatkan fitur-fitur gambar untuk pemrosesan lebih lanjut.

2. Feature Extraction

Fitur-fitur dari digit-digit tersebut diekstraksi menggunakan algoritma CNN. Arsitektur model CNN yang diusulkan dikonfigurasi dengan cara yang mirip dengan VGGNet. Namun, kami hanya menggunakan enam lapisan konvolusi dibandingkan dengan VGGNet yang memiliki setidaknya 13 lapisan. Dua lapisan konvolusi berturut-turut diikuti oleh normalisasi batch untuk konvergensi pelatihan yang lebih cepat. Sebagai input, 54.049 gambar RGB berukuran 64 × 64 × 3 piksel disalurkan ke dalam model dengan ukuran batch 32. Model tersebut dilatih dan diuji dengan dua ukuran filter yang berbeda, max-pooling dengan dua stride, ReLU, dan fungsi aktivasi SoftMax. Pada akhirnya, model yang dilatih tersebut disimpan.

3. Image Recognition

Model yang telah dilatih dimuat pada sebuah laptop menggunakan TensorFlow sebagai backend, OpenCV untuk membaca frame video, Visual Studio Code, dan Python sebagai editor dan bahasa pemrograman, secara berturut-turut. OpenCV menangkap frame video tangan secara real-time dari penandatangan dan mengubah ukurannya menjadi 64 × 64 × 3 piksel. Model tersebut berhasil mendeteksi dan memprediksi digit tanda. Model yang telah dilatih digunakan untuk memprediksi huruf-huruf ARSL secara real-time menggunakan webcam.

### Evaluation

Untuk melatih model, digunakanlah GPU online Google Collab. Model-model Bahasa Isyarat yang berbeda dilatih dengan TensorFlow sebagai backend setelah membagi dataset menjadi set pelatihan dan pengujian masing-masing sebesar 80% dan 20% dengan ukuran batch 32. Kinerja sistem pengenalan ARSL dievaluasi dengan lima Bahasa Isyarat menggunakan model jaringan CNN. 

Akurasi yang dihasilkan menggunakan tensorflow.keras seperti yang terdapat dalam notebook model di Google Collab setelah melatih model dengan jumlah yang ditentukan. Selain itu, data ditambahkan selama pelatihan untuk menghitung akurasi dan kerugian model yang dihasilkan setelah pelatihan dengan ukuran batch yang ditentukan.

Hasil evaluasi menunjukkan bahwa model CNN yang dikembangkan berhasil mencapai akurasi yang baik dalam pengenalan Bahasa Isyarat Arab. Pengujian dilakukan menggunakan real-time video melalui webcam, dan model mampu mendeteksi dan memprediksi digit dan huruf Bahasa Isyarat dengan baik.

#### Deployment

#### Model Arabic Sign Language dipasang pada aplikasi Pembelajaran Huruf Hijaiyah

# No 10

Link Google Docs : [Implementasi Metode Convolutional Neural Network Dalam Pengenalan Bahasa Isyarat Sebagai Metode Pembelajaran Huruf Hijaiyah](https://docs.google.com/document/d/1aiTjGSTOjYE-kzn13I2S47Zcr-m7aRjYdls2akdTzeE/edit?usp=sharing)
