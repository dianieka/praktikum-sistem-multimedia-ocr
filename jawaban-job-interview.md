# No 1
![Demo Aplikasi Web OCR - Image To Text](https://gitlab.com/dianieka/praktikum-sistem-multimedia-ocr/-/raw/main/OCR_-_Image_To_Text.gif)

Source Code : https://github.com/dianiekaputri/Optical-Character-Recognition.git

Video Demo : https://youtu.be/Ho9Q0dQrARw

# No 2
Aplikasi web yang saya buat berfokus pada Optical Character Recognition (OCR), untuk program image-nya adalah Image To Text.

### Optical Character Recognition (OCR)
OCR yaitu teknologi untuk mengenali karakter pada gambar dan mengubahnya menjadi teks yang dapat diedit. Hasil dari proses OCR adalah berupa text sesuai dengan gambar output scanner dimana tingkat keakuratan penerjemahan karakter tergantung dari tingkat kejelasan gambar.

### Image Processing
Pengolahan citra (Image processing) adalah suatu sistem dimana proses dilakukan dengan masukan (input) berupa citra (image) dan hasilnya (output) juga bias berupa tulisan (text).

### Image Compression
Kompresi gambar adalah proses untuk mengurangi ukuran file gambar digital tanpa kehilangan kualitas visual yang signifikan. Adapun fitur kompresi gambar digunakan setelah user mengupload gambar ke sistem. Pada proses kompresi ini, digunakan mode warna RGB pada library OpenCV dan gambar disimpan dalam format JPEG dengan kualitas 50 yang mengindikasikan bahwa gambar akan mengalami kompresi yang signifikan untuk mengurangi ukuran file, tetapi ada kemungkinan ada sedikit penurunan kualitas gambar yang dapat dilihat secara subjektif.

### Image-to-Text - library PyTesseract
Proses dalam PyTesseract/Tesseract-OCR sebenarnya akan mengambil segmentasi teks secara mandiri. Gambar yang tersegmentasi perlu dilakukan prapengolahan sesuai kebutuhan. Setelah itu, akan diambil cirinya menggunakan ekstraksi ciri sehingga mendapatkan karakteristik tiap karakter untuk mendeskripsikan sebuah obyek dalam proses pengenalan karakter. Setelah masuk ke dalam mesin dari PyTesseract maka akan dihasilkan sebuah teks sebagai keluaran dari PyTesseract.

### Alur Kerja

<div class="center">

```mermaid
flowchart LR
    A([Start]) --> B(Upload Image)
    B --> C(Preprocess Image)
    C --> D(Apply OCR)
    D --> E(Extract Text)
    E --> F(Post-process Text)
    F --> G(Output Text)
    G --> H([End])

    subgraph OCR Process
        C -->|1. Reduce File Size| I(Image Compression)
        I --> C
        D -->|2. Recognize Characters| J(Character Recognition)
        J --> D
        E -->|3. Clean up Text| K(Text Cleanup)
        K --> E
        F -->|4. Format Text| L(Text Formatting)
        L --> F
    end
```
</div>

### Deskripsi Alur Kerja dalam Flowchart

Flowchart ini menggambarkan alur kerja untuk memproses gambar, mengenali teks (OCR), dan menghasilkan output teks. Berikut adalah deskripsi langkah-langkah dalam flowchart:

1. Langkah A: Pengguna memulai dengan mengunggah gambar.
2. Langkah B: Gambar diunggah dan diproses sebelum diteruskan ke OCR.
3. Langkah C: Gambar dipreproses, termasuk langkah-langkah seperti pengurangan ukuran file menggunakan kompresi gambar.
4. Langkah D: OCR (Optical Character Recognition) diterapkan pada gambar yang telah dipreproses. Proses ini bertujuan untuk mengenali karakter-karakter teks dalam gambar.
5. Langkah E: Setelah teks diekstraksi dari gambar, teks tersebut diproses lebih lanjut dalam langkah "Clean up Text" (membersihkan teks dari kesalahan atau karakter yang tidak diinginkan).
6. Langkah F: Teks yang telah dibersihkan kemudian dipost-proses dalam langkah "Format Text" (formatting teks sesuai dengan kebutuhan atau standar tertentu).
7. Langkah G: Teks yang telah diproses selanjutnya ditampilkan sebagai output teks.
8. Langkah H: Alur kerja selesai, dan program mencapai titik akhir ([End]).

Flowchart ini memberikan gambaran visual tentang alur kerja yang terjadi dalam proses pemrosesan gambar, pengenalan teks, dan output teks.


Video Demo : https://youtu.be/Ho9Q0dQrARw

# No 3

### Optical Character Recognition (OCR)
OCR adalah kependekan dari Optical Character Recognition (Pengenalan Karakter Optik). Ini adalah teknologi yang digunakan untuk mengenali dan mengekstrak teks dari gambar atau dokumen yang tidak dapat diedit secara langsung. OCR bekerja dengan mengubah teks yang tercetak atau ditulis tangan menjadi format yang dapat diperlakukan secara digital.

Proses OCR dimulai dengan memindai atau mengambil gambar dokumen yang ingin diolah. Kemudian, OCR menganalisis gambar tersebut untuk mengenali pola dan bentuk karakter. Dengan menggunakan kecerdasan buatan, OCR dapat memahami teks dalam bahasa aslinya, mengidentifikasi struktur bahasa, dan menerjemahkannya ke dalam bahasa yang diinginkan. Setelah karakter dikenali, teks tersebut dapat diedit, dicari, atau diekstrak untuk keperluan pengolahan data lebih lanjut.

Dengan demikian, OCR memberikan kemampuan untuk mengubah teks yang tercetak atau ditulis tangan menjadi teks yang dapat diolah komputer, memfasilitasi digitalisasi dokumen, dan mendukung berbagai aplikasi yang memerlukan ekstraksi informasi dari gambar atau dokumen.

# No 4
![Demo Aplikasi OCR - Image To Audio](https://gitlab.com/dianieka/praktikum-sistem-multimedia-ocr/-/raw/main/OCR_-_Image_To_Audio.gif)

Source Code : https://github.com/dianiekaputri/Optical-Character-Recognition.git

Link Demo

Part I : https://youtu.be/Ho9Q0dQrARw 

Part II : https://youtu.be/vkbkYldSDik

# No 5
Dalam aplikasi Optical Character Recognition, program audio-nya adalah Image-to-Audio menggunakan library seperti Flask, gTTS (Google Text-to-Speech), dan FFmpeg untuk memanipulasi teks dan audio dalam aplikasi web Flask.

### Audio Processing
Audio processing adalah bidang yang berkaitan dengan pemrosesan dan manipulasi sinyal audio. Tujuan dari audio processing adalah untuk mengubah, meningkatkan, atau menganalisis sinyal audio dengan menggunakan teknik komputasi. Adapun audio processing pada text-to-speech (TTS) hasil translate melibatkan konversi teks yang diterjemahkan menjadi suara yang dapat didengar. 

### Audio Compression
Audio compression adalah proses mengurangi ukuran file audio dengan mengurangi jumlah data yang digunakan untuk merepresentasikan suara.Tujuan utama dari audio compression adalah mengurangi ukuran file audio agar lebih efisien dalam penyimpanan dan transmisi.

Aplikasi ini menggunakan library ffmpeg untuk melakukan kompresi audio dan metode kompresi-nya yaitu AAC (Advanced Audio Coding) yang merupakan format kompresi audio lossy, yang berarti beberapa informasi audio dihilangkan untuk mengurangi ukuran file. 

### Pengolahan Audio
- Equalizer: Equalizer digunakan untuk mengatur karakteristik frekuensi audio. Dalam konteks ini, equalizer digunakan untuk mengubah karakteristik audio teks yang telah diterjemahkan. Dengan mengatur frekuensi tengah, lebar, dan gain, equalizer dapat mempengaruhi kekuatan dan keseimbangan suara pada berbagai rentang frekuensi. Penggunaan equalizer dapat membantu meningkatkan kualitas audio, menyesuaikan intensitas suara, atau menghilangkan ketidakseimbangan frekuensi yang tidak diinginkan.
- Pitch Shift : Mengacu pada modifikasi tinggi rendahnya nada dalam audio. Proses pitch shift memanipulasi frekuensi sinyal audio sehingga menghasilkan suara yang lebih tinggi atau lebih rendah dari nada aslinya. Pitch shift sering digunakan dalam berbagai konteks, seperti produksi musik, pengolahan suara, dan efek audio. Dalam konteks ini, perubahan pitch digunakan untuk menciptakan variasi suara atau efek tertentu dalam audio teks yang telah diterjemahkan. Dengan meningkatkan atau menurunkan pitch, audio dapat menghasilkan nada yang lebih tinggi atau lebih rendah dari nada aslinya. Perubahan pitch dapat memberikan efek dramatis, humor, atau menyesuaikan audio dengan kebutuhan atau preferensi pengguna.

### Translate - library googletrans
Library googletrans adalah sebuah library Python yang digunakan untuk mengakses layanan terjemahan dari Google Translate menggunakan kode Python. Library ini menyediakan fungsi-fungsi yang memungkinkan pengguna untuk melakukan terjemahan teks dari satu bahasa ke bahasa lain.

Adapun cara kerja fitur ini adalah menggunakan teks yang dihasilkan pada pemrosesan OCR. Kemudian, pengguna dapat memilih bahasa yang diinginkan untuk menerjemahkan teks tersebut.

### Text-to-Speech (TTS)
TTS adalah teknologi yang mengubah suatu teks menjadi ucapan secara otomatis melalui fonetisasi (penyusunan fonem-fonem untuk membentuk ucapan). Sebuah sistem TTS dapat mengucapkan kata apapun, sebab kosa katanya tidak terbatas.

### Alur Kerja

<div class="center">

```mermaid
flowchart LR
    A([Start]) --> B(Upload Image)
    B --> C(Preprocess Image)
    C --> D(Apply OCR)
    D --> E(Extract Text)
    E --> F(Post-process Text)
    F --> G(Output Text)
    G --> T(Translate Text)
    T --> U(Compress Audio)
    U --> S(Text-to-Speech)
    S --> H([End])

    subgraph OCR Process
        C -->|1. Reduce File Size| I(Image Compression)
        I --> C
        C -->|2. Preprocess Image| J(Preprocess Image)
        J --> D
        D -->|3. Apply OCR| K(Optical Character Recognition)
        K --> E
        E -->|4. Clean up Text| L(Text Cleanup)
        L --> F
        F -->|5. Format Text| M(Text Formatting)
        M --> G
    end

    subgraph Translation Process
        G -->|6. Translate Text| T(Translate Text)
        T --> U
    end

    subgraph Audio Compression Process
        U -->|7. Compress Audio| V(Audio Compression)
        V --> U
        U -->|8. Apply Equalizer| W(Equalizer)
        W --> U
        U -->|9. Apply Pitch Shift| X(Pitch Shift)
        X --> U
    end

    subgraph Text-to-Speech Process
        T --> S(Text-to-Speech)
        S --> U
    end
```
</div>


### Deskripsi Alur Kerja dalam Flowchart

Flowchart ini menggambarkan alur kerja untuk memproses gambar, mengenali teks (OCR), menerjemahkan teks, dan menghasilkan output suara menggunakan teks yang telah diterjemahkan. Berikut adalah deskripsi langkah-langkah dalam flowchart:

1. Langkah A: Pengguna memulai dengan mengunggah gambar.
2. Langkah B: Gambar diunggah dan diproses sebelum diteruskan ke OCR.
3. Langkah C: Gambar dipreproses, termasuk langkah-langkah seperti pengurangan ukuran file menggunakan kompresi gambar.
4. Langkah D: OCR (Optical Character Recognition) diterapkan pada gambar yang telah dipreproses. Proses ini bertujuan untuk mengenali karakter-karakter teks dalam gambar.
5. Langkah E: Setelah teks diekstraksi dari gambar, teks tersebut diproses lebih lanjut dalam langkah "Clean up Text" (membersihkan teks dari kesalahan atau karakter yang tidak diinginkan).
6. Langkah F: Teks yang telah dibersihkan kemudian dipost-proses dalam langkah "Format Text" (formatting teks sesuai dengan kebutuhan atau standar tertentu).
7. Langkah G: Teks yang telah diproses selanjutnya ditampilkan sebagai output teks.
8. Langkah T: Pengguna dapat memilih untuk menerjemahkan teks menggunakan langkah "Translate Text". Teks yang diterjemahkan dapat digunakan untuk berbagai keperluan, seperti memahami teks dalam bahasa yang berbeda.
9. Langkah U: Setelah proses terjemahan, teks yang telah diterjemahkan akan mengalami dua tahap pemrosesan audio tambahan. Pertama, teks tersebut akan dikompresi dalam langkah "Compress Audio" untuk mengurangi ukuran file audio. Selanjutnya, akan dilakukan pengolahan audio yang meliputi penggunaan equalizer dan perubahan pitch.
Pertama, audio teks yang telah diterjemahkan akan diubah menjadi audio yang lebih lambat dengan mengurangi kecepatan menjadi 0.7 (70% dari kecepatan asli). Selanjutnya, akan diterapkan equalizer dengan pengaturan frekuensi tengah sebesar 1200, lebar 50, dan gain 4. Langkah ini bertujuan untuk mengubah karakteristik audio agar sesuai dengan preferensi atau kebutuhan tertentu.
Selain itu, juga akan dilakukan perubahan pitch pada audio menggunakan efek "rubberband" dengan nilai pitch sebesar 2 (naik 2 oktaf). Hal ini akan mengubah tinggi rendahnya nada dalam audio untuk menciptakan variasi atau efek suara yang diinginkan.
10. Langkah S: Teks yang telah diterjemahkan dan dikompresi kemudian diubah menjadi suara menggunakan langkah "Text-to-Speech". Proses ini menghasilkan output suara berdasarkan teks yang telah diproses sebelumnya.
11. Langkah H: Hasil akhir dari alur kerja adalah output suara yang dihasilkan dari teks asli yang telah diterjemahkan dan dikompresi.

Flowchart ini menyediakan gambaran visual tentang alur kerja yang terjadi dalam proses pemrosesan gambar, pengenalan teks, terjemahan teks, dan generasi output suara berdasarkan teks yang telah diterjemahkan.


Video Demo : https://youtu.be/Ho9Q0dQrARw

# No 6

### Text-to-Speech
Text to speech (TTS) adalah sebuah sistem berbasis artificial intelligence (kecerdasan buatan) yang mampu mengubah teks menjadi suara secara cepat dan tepat. Proses TTS dimulai dengan mengambil teks sebagai input.

Adapun proses TTS melibatkan beberapa langkah utama:

1. Analisis Teks: Teks yang diberikan diproses oleh sistem AI untuk memahami struktur kalimat, tata bahasa, dan arti kata. Hal ini dapat melibatkan pemrosesan linguistik komputasional, analisis sintaksis, dan pemahaman semantik.
2. Pemilihan Suara: Sistem TTS kemudian memilih suara atau karakter vokal yang sesuai untuk mengungkapkan teks. Suara ini dapat berupa suara manusia yang telah direkam sebelumnya atau suara sintetis yang dibuat berdasarkan sampel suara manusia.
3. Pembangkitan Ucapan: Setelah memahami teks dan memilih suara yang tepat, sistem TTS menggunakan teknik sintesis suara untuk menghasilkan suara manusia yang terdengar alami. Ada beberapa pendekatan yang digunakan dalam sintesis suara, termasuk sintesis konkatenatif (menggabungkan segmen suara pendek), sintesis berbasis unit (menggunakan unit suara kecil), atau sintesis berbasis model (menggunakan model statistik atau neural network).
4. Post-processing: Setelah suara dihasilkan, dapat dilakukan langkah post-processing untuk meningkatkan keaslian dan kejelasan suara. Hal ini bisa meliputi penyesuaian intonasi, penekanan kata-kata penting, atau penerapan efek suara tertentu.
